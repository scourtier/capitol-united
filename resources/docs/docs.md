# Capitol United Cycling Club

**EDIT FREELY!** Hack this into a million pieces, flesh out details, completely
redesign it, add a bunch of questions all over it, design an options trading
platform instead for all I care -- this is revision controlled and I'll be able
to fix it. Just get your additions/thoughts on the spec in here without worrying too much.

## _Provisional_ list of Events we care about for version 0.1.0:

### README FIRST

I'm going to use the word 'event' in two senses which I hope will be easy to
distinguish from the context:

1. an 'event' in the sense of _something that happens_ -- something
   timestampable -- that we care about in the context of this application,
   cf. 'entities', which don't have timestamps, per say. The question, "When was
   Andy Trcka born?" is natural because Andy's birth is an event, but it would
   be really weird to ask, "When was Andy Trcka?" since Andy is an entity.
   
   _In my experience_ initial design in terms of application-relevant events is
   way less confusing and way more focused, and an entity spec will basically
   design itself as a byproduct. Designing in terms of entities tends to quickly
   devolve into a bunch of, "But what even really _is_ a route?" ontological
   rabbit holes. This may be because of where I work; YMMV.
    
   Obviously we'll want to collect all sorts of details about these events and
   how they relate (e.g. when a ride is added to a Cap U event, the leader
   should be designated); I'll leave those for the next version of the spec so
   everyone can grok + discuss + suggest alternatives for the high-level stuff
   in here first.

2. an event in the sense of a Cap U event... like a gathering. This sort of
   domain-level event will have many application-level events associated with
   it, e.g. the scheduling of a 'Cap U event' is itself an event in the sense of
   1.
   
Also, this might look like it ignores a bunch of features we talked about, but
ask yourself whether those features could be derived from the implied details of
the application events listed. If they can, we're on the right track. For
example, there's nothing in here about Participation %, but if we have a list of
every ride completion, the proportion of the ride completed, who completed it,
and what Cap U event it was a part of, then we can compute everyone's
Participation % over a given time span using the list of all non-canceled Cap U
event offerings.

That said, I think I'll probably leave all the commenting stuff out of 0.1.0.

Also also, suggestions for better terminology (or anything else for that matter)
are always welcome.

### Who's in Cap U?

I think we talked about distinguishing members from casuals / followers /
subscribers / general users? Seems like these could be useful for e.g. 
collecting contact info on people who are interested but not yet ready to join
in:

- Someone follows Cap U (and so becomes a general 'user' and gives us their
  profile info in the process)
- Someone unfollows Cap U
- Someone modifies their profile info

A member is someone we can shower with Minnesota-crafted, passive-aggressive
disapproval if they show up to rides too infrequently ;). They also probably own
Cap U kit. All members are also followers.

- Someone becomes a member
- Someone stops being a member

A Cap U manager is just someone with More Authority (to e.g. decide
things about how rides will go) than others. All managers are members.

- Someone becomes a Cap U manager
- Someone stops being a Cap U manager

Achievements/citations/badges are distinguished from membership classes by the
fact that they must be earned -- smooth talking won't earn you Ti, no siree,
Bob. Feel free to list off all the ideas for achievements you've got.

- Someone earns an achievement/citation/badge

And GDPR stuff:

- Someone permits us to use their data for everything obvious we'd want to do
  with it plus everything not-so-obvious, e.g. using it to figure out how to get
  more donations, or using it for Sam's mad-scientist data analyses (but we'll
  need opt in on a per-analysis basis for that).
- Someone asks for a package of all their data in an open format.
- Someone opts out and obligates us to delete everything about them from our
  system.

### What's in Cap U's route portfolio and where are the Cap U haunts?

This a parent-child model for routes that would support variations on
variations. Just having routes which can be added to 'route groups' would be
more straightforward, but I thought I'd outline the more flexible approach first.

- A route is added
- A route variation is added. A route variation is a route, viz. it's a child
  route of a parent route
- A route is modified
- A route is removed

Canonical Cap U destinations -- we talked about making arrangements with these
folks, but I think having this is also just useful for coordination in-and-of
itself:

- A rendezvous is added
- A rendezvous is modified
- A rendezvous is deleted

### What is Cap U doing with its life?

Events can be Cap U sanctioned or unsanctioned and can be restricted to
different classes of users (e.g. members only rides, carbon-or-above-only).

- A Cap U event is scheduled
- An event is modified
- An event is canceled

Events can include multiple rides, which can impose further restrictions on
participant qualifications. A single route/variation is selected for each ride.

- A ride is added to an event
- A ride is modified
- A ride is removed from an event / canceled

A user can sign up for any number of non-coincident rides (if they have the right
quals).

- A user registers for a ride
- A user unregisters for a ride

Users can sign up for rendezvous too:

- A user registers for a rendezvous
- A user unregisters for a rendezvous

And, of course, they can ride:

- A user completes (some proportion of) a ride
