(ns capitol-united.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [capitol-united.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[capitol-united started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[capitol-united has shut down successfully]=-"))
   :middleware wrap-dev})
