(ns user
  (:require [capitol-united.config :refer [env]]
            [clojure.spec.alpha :as s]
            [expound.alpha :as expound]
            [mount.core :as mount]
            [capitol-united.core :refer [start-app]]))

(alter-var-root #'s/*explain-out* (constantly expound/printer))

(defn start []
  (mount/start-without #'capitol-united.core/repl-server))

(defn stop []
  (mount/stop-except #'capitol-united.core/repl-server))

(defn restart []
  (stop)
  (start))


