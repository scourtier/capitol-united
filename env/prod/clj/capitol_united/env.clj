(ns capitol-united.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[capitol-united started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[capitol-united has shut down successfully]=-"))
   :middleware identity})
