# Capitol United

Site: https://capitol-united.herokuapp.com/

Framework: http://www.luminusweb.net/

Deploy:

```
lein uberjar
heroku container:push web -a capitol-united
heroku container:release web -a capitol-united
```
