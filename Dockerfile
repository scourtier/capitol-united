FROM openjdk:8-alpine

COPY target/uberjar/capitol-united.jar /capitol-united/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/capitol-united/app.jar"]
